# RayVox

A voxel-based GPU real-time ray tracer. Will eventually become a Minecraft-inspired game.

Build with
`cabal install`
and run with
`.cabal-sandbox/rayvox`