{-# LANGUAGE ScopedTypeVariables, TypeFamilies #-}
module Main where

import Voxel
import Terrain

import Graphics.GPipe
import qualified Graphics.GPipe.Context.GLFW as GLFW
import Control.Monad (unless)
import Data.Word (Word32)
import Control.Applicative (pure)
import Control.Lens
import Control.Concurrent.MonadIO
import System.Random
import GHC.Float -- for float2Double and double2Float

fib :: Word32 -> Word32
fib x
    | x <= 1 = 1
    | otherwise = (fib $ x - 1) + (fib $ x - 2)

main = 
  runContextT GLFW.defaultHandleConfig $ do
    let pos = V3 (-(mapSize / 2) - 4) (-(mapSize / 2) - 4) (4 :: Float)
        dir = normalize $ V3 0.5 0.5 (0 :: Float)
        up  = normalize $ V3 0 0 (1 :: Float)
        
    win <- newWindow (WindowFormatColor RGB8) (GLFW.defaultWindowConfig "Ray Marching")
    (V2 x y) <- getFrameBufferSize win
    let rat = toFloat x / toFloat y -- The aspect ratio
    vertexBuffer :: Buffer os (B2 Float, B3 Float) <- newBuffer 4
    writeBuffer vertexBuffer 0 [  (V2 (-1) (-1), calcDir (V2 (-1) (-1)) dir up rat)
                                , (V2 1 (-1), calcDir (V2 1 (-1)) dir up rat)
                                , (V2 (-1) 1, calcDir (V2 (-1) 1) dir up rat)
                                , (V2 1 1, calcDir (V2 1 1) dir up rat)]

    camerabuffer :: Buffer os (Uniform (B3 Float)) <- newBuffer 1
    writeBuffer camerabuffer 0 [pos]
    
    grid <- newTexture3D R8UI (pure mapSize) 1
    writeTexture3D grid 0 0 (pure mapSize) $ genTerrainL 3 --randomRs (0,2::Word32) (mkStdGen 3)--((take 13 $ repeat 0) ++ 1:0:1:1:repeat (0::Word32))
     
    shader <- compileShader $ do
        primitiveStream <- toPrimitiveStream primitives
        let primitiveStream2 = fmap (\((V2 x y), d) -> (V4 x y 0 1, d)) primitiveStream
        fragmentStream <- rasterize rasterOptions primitiveStream2
        let filter = SamplerNearest
            edge = (pure ClampToBorder, 0)
        samp <- newSampler3D (const (grid, filter, edge))
        camera <- getUniform (const (camerabuffer, 0))
        let nCells = sampler3DSize samp 0 
            fragmentStream2 = fmap (raytrace samp nCells camera) fragmentStream
        drawWindowColor (const (win, ContextColorOption NoBlending (pure True))) fragmentStream2
    
    GLFW.setCursorInputMode win GLFW.CursorInputMode'Disabled
    p <- GLFW.getCursorPos win
    case p of
        Just (x, y) -> loop win shader vertexBuffer camerabuffer (double2Float x, double2Float y) dir up pos
        Nothing -> loop win shader vertexBuffer camerabuffer (0, 0) dir up pos

raytrace :: Sampler3D (Format RWord) -> V3 FInt -> V3 FFloat -> V3 FFloat -> FragColor RGBFloat
raytrace samp nCells pos dir = mc mId * (pure $ occ * 0.3) + shade mId normal (pos + pure t*dir) samp'
    where (mId, normal, t, occ) = Voxel.traverse pos dir samp' nCells
          samp' = (sample3D samp SampleAuto Nothing Nothing . (/fmap toFloat nCells)) -- texelFetch is broken in GPipe

lightPos = V3 (-16) (-10) 10

normalize' (V3 x y z) = V3 (x/a) (y/a) (z/a)
    where a = sqrt (x*x + y*y + z*z)

mc mat = ifB (mat ==* 0) (pure 0) (ifB (mat ==* 1) (pure 1) (V3 0.2 0.7 0.3))

shade :: FWord -> V3 FFloat -> V3 FFloat -> Voxel.Lookup3D -> V3 FFloat
shade mat normal p samp = ifB (mat ==* 0) (V3 0.5 0.7 0.9) $ ifB (mat' ==* 0 &&* (dot normal l) >* 0) (mc mat * (pure $ dot normal l)) 0--(mc mat * (pure $ dot normal l) * (mc mat' * (pure $ dot normal' (lightPos - (p + pure t * l)))))
    where l = normalize' $ lightPos - p
          (mat', normal', t, _) = Voxel.traverse (p+pure 0.01*normal) l samp (pure 0)

dc x = distance x (V3 2 1 3) - 0.5

recM :: V3 FFloat -> V3 FFloat -> V3 FFloat
recM p dir = ifB (d <=* 0.1) (V3 1 1 1) (V3 0 0 0)
    where (d, p', r) = while (\(dis,pos,rep) -> dis >* 0.1 &&* rep <=* 64) (recM' dir) (dc p, p, 0)

recM' :: V3 FFloat -> (FFloat, V3 FFloat, FInt) -> (FFloat, V3 FFloat, FInt)
recM' dir (dis, pos, rep) = let pos' = pos+(pure dis)*dir 
                            in (dc pos', pos', rep + 1)

data ShaderEnvironment = ShaderEnvironment
  { primitives :: PrimitiveArray Triangles (B2 Float, B3 Float)
  , rasterOptions :: (Side, ViewPort, DepthRange)
  }

updateDirs dir up vertexBuffer rat = writeBuffer vertexBuffer 0 [  (V2 (-1) (-1), calcDir (V2 (-1) (-1)) dir up rat)
                                , (V2 1 (-1), calcDir (V2 1 (-1)) dir up rat)
                                , (V2 (-1) 1, calcDir (V2 (-1) 1) dir up rat)
                                , (V2 1 1, calcDir (V2 1 1) dir up rat)]

calcDir :: V2 Float -> V3 Float -> V3 Float -> Float -> V3 Float
calcDir (V2 x y) dir up rat = normalize $ dir - (pure (rat*x) * (cross dir up)) + (pure y * up)

updatePos dir up w s space shift = updateW dir w + updateS dir s + updateSp up space + updateSh up shift

updateW dir (Just GLFW.KeyState'Pressed) = dir * pure 0.01
updateW dir _                            = pure 0

updateS dir (Just GLFW.KeyState'Pressed) = -dir * pure 0.01
updateS dir _                            = pure 0

updateSp up (Just GLFW.KeyState'Pressed) = up * pure 0.01
updateSp up _                            = pure 0

updateSh up (Just GLFW.KeyState'Pressed) = -up * pure 0.01
updateSh up _                            = pure 0

loop :: Window os RGBFloat ds -> CompiledShader os ShaderEnvironment -> Buffer os (B2 Float, B3 Float) -> Buffer os (Uniform (B3 Float)) -> (Float, Float) -> V3 Float -> V3 Float -> V3 Float -> ContextT GLFW.Handle os IO ()
loop win shader vertexBuffer camerabuffer (prevX, prevY) dir up pos = do
  size@(V2 x y) <- getFrameBufferSize win
  let rat = (toFloat x) / (toFloat y)
  render $ do
    clearWindowColor win 0.5
    vertexArray <- newVertexArray vertexBuffer
    shader $ ShaderEnvironment (toPrimitiveArray TriangleStrip vertexArray) (FrontAndBack, ViewPort (V2 0 0) size, DepthRange 0 1)
  
  swapWindowBuffers win
  closeRequested <- GLFW.windowShouldClose win
  unless (closeRequested == Just True) $ do
    mpos <- GLFW.getCursorPos win
    case mpos of
        Just (x, y) -> do
            let (x', y') = (double2Float x, double2Float y)
            w <- GLFW.getKey win GLFW.Key'Comma
            s <- GLFW.getKey win GLFW.Key'O
            space <- GLFW.getKey win GLFW.Key'Space
            shift <- GLFW.getKey win GLFW.Key'LeftShift
            let xq  = axisAngle up $ 0.01 * (x' - prevX) -- Quaternions representing the camera rotation
                yq  = axisAngle (cross dir up) $ 0.01 * (- (y' - prevY))
                dir' = rotate xq $ rotate yq dir
                up'  = rotate xq $ rotate yq up
                pos' = pos + updatePos dir' up' w s space shift
            updateDirs dir' up' vertexBuffer rat
            writeBuffer camerabuffer 0 [pos']
            loop win shader vertexBuffer camerabuffer (x', y') dir' up' pos'
        Nothing     -> loop win shader vertexBuffer camerabuffer (prevX, prevY) dir up pos -- Cursor position didn't change