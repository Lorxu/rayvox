-- A Haskell implementation of _A Fast Voxel Traversal Algorithm for Ray Tracing_ by John Amanatides
{-# LANGUAGE TypeFamilies #-}

-- TODO fix artifact viewing voxels near-straight-on (two components of tMax are very high, one is very low)

module Voxel where

import Graphics.GPipe hiding ((<*))
import qualified Graphics.GPipe ((<*)) -- <* is in Prelude and GPipe, so we need to disambiguate them
import Control.Monad (zipWithM)
import Control.Lens

type Lookup3D = V3 FFloat -> FWord
type Result   = (FWord, V3 FFloat, FFloat, FFloat) -- matid, normal, t, ao

inf = 1/0 -- Infinity

sign :: FFloat -> FFloat
sign x =
    ifB (x >* 0) 1
    $ ifB (x Graphics.GPipe.<* 0) (-1) 0

minV (V3 x y z) =
    ifB (y >* x &&* z >* x) (V3 x 0 0)
    $ ifB (x >* y &&* z >* y) (V3 0 y 0)
    $ V3 0 0 z

min' (V3 x y z) =
    ifB (y >* x &&* z >* x) x
    $ ifB (x >* y &&* z >* y) y z

max' (V3 x y z) =
    ifB (x >* y &&* x >* z) x
    $ ifB (y >* x &&* y >* z) y z

smV (V3 x y z) = -- 1 in the smallest component, otherwise 0
    ifB (y >* x &&* z >* x) (V3 1 0 0)
    $ ifB (x >* y &&* z >* y) (V3 0 1 0)
    $ V3 0 0 1

round' :: FFloat -> FInt
round' x = toInt $ ifB (x Graphics.GPipe.<* 5) (floor' x) (ceiling' x)

-- Branchless DDA style traversal - based on https://www.shadertoy.com/view/4dX3zl by fb39ca4
traverse :: V3 FFloat -> V3 FFloat -> Lookup3D -> V3 FInt -> Result
traverse pos dir grid nCells = recT pos grid dir

-- From https://www.shadertoy.com/view/ldl3DS
ao :: Lookup3D -> V3 FFloat -> V3 FFloat -> V3 FFloat -> V3 FFloat -> V3 FFloat -> FFloat
ao grid pos d1 d2 pos2 m = mix (mix az aw ux) (mix ay ax ux) uy
    where ax                = 1 - vao (view _xy side) cx
          ay                = 1 - vao (view _yz side) cy
          az                = 1 - vao (view _zw side) cz
          aw                = 1 - vao (view _wx side) cw
          side              = fmap toFloat $ V4 (grid $ pos + d1) (grid $ pos + d2) (grid $ pos - d1) (grid $ pos - d2)
          (V4 cx cy cz cw)  = fmap toFloat $ V4 (grid $ pos + d1 + d2) (grid $ pos - d1 + d2) (grid $ pos - d1 - d2) (grid $ pos + d1 - d2)
          (V2 ux uy)        = mod'' (V2 (dot (m * view _yzx pos2) (pure 1)) (dot (m * view _zxy pos2) (pure 1))) (pure 1)

vao :: V2 FFloat -> FFloat -> FFloat
vao (V2 sx sy) corner = (sx + sy + maxB corner (sx * sy)) / 3

recT :: V3 FFloat -> Lookup3D -> V3 FFloat -> Result
recT pos grid dir = (grid p, -m * fmap sign dir, t p pos dir, ao grid (p - fmap sign dir * m) (view _zxy m) (view _yzx m) (pos + (pure $ t p pos dir) * dir) m)
    where (p, m, d, r) = while (\(a, _, _, b) -> b <=* 64 &&* grid a ==* 0) (nextVoxel dir) (floor' pos, pure 0, ({-fmap sign dir * -}(floor' pos - pos) + 0.5 + fmap sign dir * 0.5) * (1 / dir), 0 :: FInt)

t pos orig dir = max' mini
    where mini = (pos - orig + 0.5 - 0.5 * (fmap sign dir)) * (1 / dir)

nextVoxel dir (p, m, d, r) = (p + m' * step', m', d + m' * step' * (1 / dir), r + 1)
    where step' = fmap sign dir
          m'    = (step <$> d <*> (view _yzx d)) * (step <$> d <*> (view _zxy d))
    
{-
-- Amanatides and Woo style traversal
traverse :: V3 FFloat -> V3 FFloat -> Lookup3D -> V3 FInt -> Result
traverse pos dir grid nCells = recT grid nCells step tDelta gpos tMax
    where gpos = fmap ((0.501+) . floor') pos -- The center of the current voxel
          step = fmap sign dir -- The direction in which to increment gpos
          tMax = (gpos - pos) / dir -- t-value at the voxel border
          tDelta = 1 / (fmap abs dir) -- distance to the voxel border

recT :: Lookup3D -> V3 FInt -> V3 FFloat -> V3 FFloat -> V3 FFloat -> V3 FFloat -> Result
recT grid nCells step tDelta gpos tMax = (grid p, n, min' t)
          where (p, t, n, r) = while (\(a, _, _, c) -> c Graphics.GPipe.<* 64 &&* (grid a ==* 0)) (nextVoxel tDelta step) (gpos, tMax, -step * smV tMax, (0 :: FInt))

nextVoxel tDelta step (pos, tMax, normal, reps) = 
    (pos + stepT * step -- Increment pos in direction `stepT` according to step
    , tMax + stepT * tDelta -- Increment tMax by tDelta in the direction
    , -step * stepT -- hit normal
    , reps + 1)
    where stepT = smV tMax -- The direction to increment-}