-- Terrain generator
module Terrain where

import Data.Word (Word32)
import Graphics.GPipe

mapSize :: Num a => a
mapSize = 32

genTerrain :: Int -> Int -> Word32
genTerrain seed el
    | z < h     = 1
    | z == h    = 2
    | otherwise = 0
    where el3@(V3 x y z) = V3 (el `mod` mapSize) ((el `div` mapSize) `mod` mapSize) (((el `div` mapSize) `div` mapSize) `mod` mapSize)
          h              = floor $ sin (0.02 * (toFloat $ seed * x + seed * y)) * (mapSize / 3)
          
genTerrainL :: Int -> [Word32]
genTerrainL seed = [genTerrain seed x | x <- [0..]]